﻿using Exame.Bexs.FullStack.Domain.Entities;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

namespace Exame.Bexs.FullStack.Dal
{
    public class ExameBexsFullStackContext : DbContext
    {
        private static string _connectionString;
        private static ExameBexsFullStackContext _instance;
        private static bool _loaded;

        public static void SetConnectionString(string newConnectionString)
        {
            _connectionString = newConnectionString;
            if (!_loaded)
            {
                _instance = new ExameBexsFullStackContext();
                _loaded = true;
            }
        }

        public static ExameBexsFullStackContext GetInstance()
        {
            return _instance;
        }

        protected static System.Data.Common.DbConnection GetDatabase()
        {
            return new SqlConnection(_connectionString);
        }

        internal ExameBexsFullStackContext() : base("Connection")
        {
        }

        public DbSet<Questioning> Questionings { get; set; }
        public DbSet<Answer> Answers { get; set; }
    }
}
