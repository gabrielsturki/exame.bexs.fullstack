﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exame.Bexs.FullStack.Domain.Entities
{
    [Table("Questioning")]
    public class Questioning
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("Text")]
        public string Text { get; set; }

        [Column("User")]
        public string User { get; set; }

        [Column("CreationDate")]
        public DateTime CreationDate { get; set; }

    }
}
