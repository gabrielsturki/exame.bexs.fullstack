﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exame.Bexs.FullStack.Domain.Entities
{
    [Table("Answer")]
    public class Answer
    {
        [Column("Id")]
        public int Id { get; set; }
        public string Text { get; set; }
        public string User { get; set; }
        public DateTime CreationDate { get; set; }

        public int QuestioningId { get; set; }

        [ForeignKey("QuestioningId")]
        public virtual Questioning Questioning { get; set; }

    }
}
