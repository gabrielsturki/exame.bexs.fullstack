﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exame.Bexs.FullStack.Domain.DTO
{
    public class DateQuestioningDto
    {
        public DateQuestioningDto()
        {
            Questionings = new List<QuestioningDto>();
        }
        public string CreationDate { get; set; }

        public List<QuestioningDto> Questionings { get; set; }
    }
}
