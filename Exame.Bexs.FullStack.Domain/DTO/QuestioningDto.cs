﻿using System;
using System.Collections.Generic;

namespace Exame.Bexs.FullStack.Domain.DTO
{
    public class QuestioningDto
    {
        public QuestioningDto()
        {
            Answers = new List<AnswerDto>();
        }
        public int Id { get; set; }
        public string User { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }

        public List<AnswerDto> Answers { get; set; }
    }
}
