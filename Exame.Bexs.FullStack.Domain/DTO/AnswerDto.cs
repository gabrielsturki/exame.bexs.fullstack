﻿using System;

namespace Exame.Bexs.FullStack.Domain.DTO
{
    public class AnswerDto
    {
        public int AnswerId { get; set; }
        public string User { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }

        public int QuestioningId { get; set; }
    }
}
