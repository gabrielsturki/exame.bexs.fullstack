﻿define(function () {
    var appInfoService = function ($http, $q) {
        var appInfo = {};

        appInfo.obterDadosServidor = function () {
            return $http.get('Api/AppInfo', { notification: false })
        };

        appInfo.obterPermissoes = function () {
            var deferred = $q.defer();

            $http.get('Api/AppPermissoes', { notification: false })
                .then(function (response) {
                    var permissoes = response.data;

                    if (!Array.isArray(permissoes)) {
                        console.warn('O valor retornado não é uma coleção válida de permissões.');
                        permissoes = [];
                    }

                    deferred.resolve(response.data);
                })
                .catch(function () {
                    console.warn('Não foi possível obter as permissões do usuário.');
                    deferred.resolve([]);
                });

            return deferred.promise;
        };

        appInfo.obterMenus = function () {
            return $http.get('Api/AppMenu')
        }

        return appInfo;
    };

    return appInfoService;
});