﻿define(function () {
    var todoController = function ($scope) {
        $scope.total = 3;
        $scope.totalPendentes = 12;
        $scope.tarefas = [
            { percentual: 30.12, tipo: 'progress-bar-success', descricao: 'New release v1.2' },
            { percentual: 65.45, tipo: 'progress-bar-danger', descricao: 'Application deployment' },
            { percentual: 98.23, tipo: 'progress-bar-success', descricao: 'Mobile app release' },
            { percentual: 10.65, tipo: 'progress-bar-warning', descricao: 'Database migration' },
            { percentual: 58.98, tipo: 'progress-bar-info', descricao: 'Web server upgrade' },
            { percentual: 85.34, tipo: 'progress-bar-success', descricao: 'Mobile development' },
            { percentual: 38.12, tipo: 'progress-bar-important', descricao: 'New UI release' }
        ];

        $scope.formatarPencentual = function (valor) {
            return valor + "%";
        };
    };

    return todoController;
});