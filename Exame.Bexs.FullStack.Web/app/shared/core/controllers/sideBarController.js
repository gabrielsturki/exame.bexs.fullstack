﻿define(function () {
    var sideBarController = function ($rootScope, $scope, $window, appInfoService) {
        var permissoes = [];

        // -----------------------------
        // funcoes privadas
        // -----------------------------
        function verificarPermissoes(menus) {
            if (!menus) return undefined;

            return menus.map(verificarPermissaoMenu);
        }

        function verificarPermissaoMenu(item) {
            if (item.permissao) {
                var autorizado = false;

                if (permissoes.length > 0) {
                    autorizado = permissoes.some(p => p == item.permissao);
                }

                item.desabilitado = !autorizado;
            }

            if (item.menus) {
                item.menus = verificarPermissoes(item.menus);
            }

            return item;
        }

        function obterMenus(configuracoes) {
            if (configuracoes.menuCustomizado) {
                require(['app/custom/config/appMenu.js'], configurarMenus);
                return;
            }

            // Busca os menus do usuário.
            appInfoService.obterMenus()
                .then(function (responseMenu) {
                    var appMenus = responseMenu.data;

                    configurarMenus(appMenus)
                });
        }

        function configurarMenus(appMenus) {
            // busca as permissoes ativas para o usuário
            appInfoService.obterPermissoes()
                .then(function (response) {
                    permissoes = response;

                    // verificar permissoes
                    $scope.menus = verificarPermissoes(appMenus);

                    // Armazena os menus numa variável isolada, para que quando for disparado o evento de troca de tipo de menu, o original esteja guardado.
                    $scope.listaMenus = angular.copy($scope.menus);
                });
        }

        var configurarMenuBarraLateral = function (config) {
            if (!config.sideBarMenu) return;
            $scope.sideBarMenu = config.sideBarMenu == 'hover' ? 'page-sidebar-menu-hover-submenu' : '';
        };

        var configurarEstiloBarraLateral = function (config) {
            if (!config.sideBarStyle) return;
            $scope.sideBarStyle = config.sideBarStyle == 'light' ? 'page-sidebar-menu-light' : '';
        };

        // -----------------------------
        // funções publicas
        // -----------------------------
        $scope.highlightMenu = function (item) {
            $scope.menus.forEach(function (menu) {
                if (item.titulo == menu.titulo) {
                    menu.active = true;
                } else {
                    menu.active = false;
                }
            });
        };

        $scope.gotoPage = function (item) {
            if (!item) return false;
            if (!item.url) return false;
            if (item.desabilitado) return false;

            if (!item.menus) {
                $window.scrollTo(0, 0);
            }

            $window.location.href = item.url;
        };

        // -----------------------------
        // eventos
        // -----------------------------
        $rootScope.$on('cbCustomChanged', function (event, args) {
            configurarMenuBarraLateral(args);
            configurarEstiloBarraLateral(args);
        });

        $rootScope.$on("trocarTipoMenu", function (event, args) {
            var tipoMenu = args;

            if (tipoMenu == 1) {
                $scope.menus = $scope.listaMenus;
            }
            else {
                var menuSistema = $scope.listaMenus.find(menuSistema => menuSistema.principal);
                $scope.menus = [menuSistema];
            }
        });

        // -----------------------------
        // inicialização do controller
        // -----------------------------
        this.$onInit = function () {
            $scope.sideBarMenu = "";
            $scope.sideBarStyle = "";

            require(['app/custom/config/appConfig.js'], obterMenus);
        };
    };

    return sideBarController;
});