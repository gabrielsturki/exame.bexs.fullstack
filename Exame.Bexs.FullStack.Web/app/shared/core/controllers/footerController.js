﻿define(function (require) {

    var appConfig = require('app/custom/config/appConfig.js');
    var footerController = function ($scope) {
        $scope.tituloAplicacao = appConfig.titulo || 'Título da Aplicação';
        $scope.anoAtual = new Date().getFullYear();
    };

    return footerController;
});