﻿define(function () {
    var notificationController = function ($scope) {
        $scope.total = 8;
        $scope.totalPendentes = 12;
        $scope.notificacoes = [
            { tipo: 'label-success', icone: 'fa-plus', hora: 'just now', descricao: 'New user registered 1.' },
            { tipo: 'label-danger', icone: 'fa-bolt', hora: '3 mins', descricao: 'Server #12 overloaded.' },
            { tipo: 'label-warning', icone: 'fa-bell-o', hora: '10 mins', descricao: 'Server #2 not responding.' },
            { tipo: 'label-info', icone: 'fa-bullhorn', hora: '14 hrs', descricao: 'Application error.' },
            { tipo: 'label-danger', icone: 'fa-bolt', hora: '2 days', descricao: 'Database overloaded 68%.' },
            { tipo: 'label-danger', icone: 'fa-bolt', hora: '3 days', descricao: 'A user IP blocked.' },
            { tipo: 'label-warning', icone: 'fa-bell-o', hora: '4 days', descricao: 'Storage Server #4 not responding.' },
            { tipo: 'label-info', icone: 'fa-bullhorn', hora: '5 days', descricao: 'System Error.' },
            { tipo: 'label-danger', icone: 'fa-bolt', hora: '9 days', descricao: 'Storage server failed.' }
        ];
    };

    return notificationController;
});