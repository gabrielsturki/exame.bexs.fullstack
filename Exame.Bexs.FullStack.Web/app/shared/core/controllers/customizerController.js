﻿define(function () {

    var customizerController = function ($rootScope, $scope) {

        // -----------------------------------
        // inicialização do controller
        // -----------------------------------
        this.$onInit = function () {
            var userConfig = {};
            var cbCustomizerConfig = window.localStorage.getItem('cb.Customizer');

            if (cbCustomizerConfig) {
                userConfig = JSON.parse(cbCustomizerConfig);
            }

            // obter valores iniciais.. 
            $scope.config = {
                themeColor: userConfig.themeColor || 'light2',
                themeStyle: userConfig.themeStyle || 'square',
                pageLayout: userConfig.pageLayout || 'fluid',
                pageHeader: userConfig.pageHeader || 'default',
                topMenu: userConfig.topMenu || 'dark',
                sideBar: userConfig.sideBar || 'fixed',
                sideBarMenu: userConfig.sideBarMenu || 'accordion',
                sideBarStyle: userConfig.sideBarStyle || 'default',
                sideBarPosition: userConfig.sideBarPosition || 'left',
                footerStyle: userConfig.footerStyle || 'fixed'
            };
        };

        $scope.changeThemeColor = function (themeColor) {
            $scope.config.themeColor = themeColor;
        };

        $scope.$watch('config', function (newVal, oldVal, scope) {
            localStorage.setItem('cb.Customizer', JSON.stringify(newVal));

            $rootScope.$emit('cbCustomChanged', newVal);
        }, true);

    };

    return customizerController;
});