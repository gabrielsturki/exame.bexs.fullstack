﻿define(function () {
    var inboxController = function ($scope) {
        $scope.total = 5;
        $scope.totalNovas = 7;
        $scope.mensagens = [
            { imagem: 'assets/img/avatar2.jpg', remetente: 'Lisa Wong', hora: 'Just Now', mensagem: 'Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh...' },
            { imagem: 'assets/img/avatar3.jpg', remetente: 'Richard Doe', hora: '16 mins', mensagem: 'Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh...' },
            { imagem: 'assets/img/avatar1.jpg', remetente: 'Bob Nilson', hora: '40 mins', mensagem: 'Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh...' },
            { imagem: 'assets/img/avatar2.jpg', remetente: 'Lisa Wong', hora: '52 mins', mensagem: 'Vivamus sed auctor 40% nibh congue nibh...' },
            { imagem: 'assets/img/avatar3.jpg', remetente: 'Richard Doe', hora: '2 hrs', mensagem: 'Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh...' }
        ];
    };

    return inboxController;
});