﻿define(function () {
    var bodyController = function ($rootScope, $scope, $state) {
        $scope.showQuickSidebar = false;
        $scope.conta;

        $scope.toggleQuickSidebar = function () {
            $scope.showQuickSidebar = !$scope.showQuickSidebar;
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name == "home") {
                $scope.homePageCss = false
            }

            if (toState.name == "home") {
                $scope.homePageCss = true
            }
        });

        var alterarCorDoTema = function (config) {
            if (!config.themeColor) return;

            $('#style_color').attr("href", 'assets/css/themes/' + config.themeColor + ".css");
        };

        var alterarEstiloDoTema = function (config) {
            if (!config.themeStyle) return;

            var fileSufix = config.themeStyle == 'rounded' ? '-rounded' : '';

            $('#style_components').attr("href", 'assets/css/components' + fileSufix + ".css");
        };

        $rootScope.$on('cbCustomChanged', function (event, args) {
            alterarCorDoTema(args);
            alterarEstiloDoTema(args);
        });
    };

    return bodyController;
});