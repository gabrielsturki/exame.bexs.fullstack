﻿define(function (require) {

    var appConfig = require('app/custom/config/appConfig.js');
    var homeController = function ($scope) {
        $scope.appConfig = appConfig;
    };

    return homeController;
});