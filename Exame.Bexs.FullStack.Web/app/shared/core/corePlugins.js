﻿define(function (require) {

    var appPlugins = require('app/custom/config/appPlugins.js');
    var corePaths = {
        // -- app --
        'app': 'assets/scripts/appStart',

        'appModules': 'app/custom/config/appModules.js',
        'componentBase': 'app/shared/componentBase',
        'controllerBase': 'app/shared/controllerBase',

        // -- angular modules --
        'commonModule': 'app/shared/common/commonModule',
        'coreModule': 'app/shared/core/coreModule',

        // -- template --
        'metronic': 'assets/scripts/metronic',
        'layout': 'assets/scripts/layout',
        'quick-sidebar': 'assets/scripts/quick-sidebar',
        'demo': 'assets/scripts/demo',

        // -- libs --
        'jquery': 'assets/plugins/jquery.min',
        'bootstrap': 'assets/plugins/bootstrap/js/bootstrap.min',
        'bootstrap-hover-dropdown': 'assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
        'bootstrap-switch': 'assets/plugins/bootstrap-switch/js/bootstrap-switch.min',
        'bootstrap-datepicker': 'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min',
        'bootstrap-datetimepicker': 'assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
        'bootstrap-datetimepicker-ptBR': 'assets/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR',
        'jquery-migrate': 'assets/plugins/jquery-migrate.min',
        'jquery.blockui': 'assets/plugins/jquery.blockui.min',
        'jquery.cokie': 'assets/plugins/jquery.cokie.min',
        'jquery.uniform': 'assets/plugins/uniform/jquery.uniform.min',
        'jquery.slimscroll': 'assets/plugins/jquery-slimscroll/jquery.slimscroll.min',
        'jquery-ui': 'assets/plugins/jquery-ui/jquery-ui.min',
        'jquery.datatables': 'assets/plugins/jquery.datatables/jquery.datatables.min',
        'datatables.bootstrap': 'assets/plugins/jquery.datatables/datatables.bootstrap',
        'select2': 'assets/plugins/select2/select2.min',
        'select2_ptBR': 'assets/plugins/select2/select2_locale_pt-BR',
        'bootstrap-select': 'assets/plugins/bootstrap-select/bootstrap-select.min',
        'angular': 'assets/plugins/angular/angular.min',
        'angular-ptbr': 'assets/plugins/angular/i18n/angular-locale_pt-br',
        'ngAnimate': 'assets/plugins/angular/angular-animate.min',
        'ui.router': 'assets/plugins/angular-ui-router/angular-ui-router.min',
        'ngResource': 'assets/plugins/angular/angular-resource.min',
        'angular-loading-bar': 'assets/plugins/angular-loading-bar/js/loading-bar.min',
        'LocalStorageModule': 'assets/plugins/angular-local-storage/angular-local-storage.min',
        'angular-ui-select': 'assets/plugins/angular-ui-select/select.min',
        'ngSanitize': 'assets/plugins/angular/angular-sanitize.min',
        'datatables': 'assets/plugins/angular-datatables/angular-datatables.min',
        'ngNotify': 'assets/plugins/ng-notify/ng-notify.min',
        'fileSaver': 'assets/plugins/fileSaver/FileSaver',
        'oidc': 'assets/plugins/oidc-client/oidc-client.min',
        'angular-ui-mask': 'assets/plugins/angular-ui-mask/mask.min',
        'angular-input-masks': 'assets/plugins/angular-input-masks/angular-input-masks-webpack.min',
        'angular-br-filters': 'assets/plugins/angular-br-filters/angular-br-filters-webpack.min',
    };

    // inclui as demais configurações de arquivos específicos da aplicação
    for (var i = 0; i < appPlugins.length; i++) {
        var item = appPlugins[i];
        corePaths[item.name] = item.path;
    }
    // --------------------------------------------------------------

    var commonDeps = [
        'ngResource',
        'ui.router',
        'ngNotify',
        'ngSanitize',
        'ngAnimate',
        'angular-loading-bar',
        'datatables',
        'LocalStorageModule',
        'angular-input-masks',
        'angular-br-filters',
        'angular-ui-mask',
    ];

    // inclui as demais configurações de arquivos específicos da aplicação
    for (var j = 0; j < appPlugins.length; j++) {
        commonDeps.push(appPlugins[j].name);
    }

    var coreShim = {
        // -- app --
        'app': {
            deps: [
                'bootstrap-datepicker',
                'bootstrap-datetimepicker-ptBR',
                'bootstrap-select',
                'quick-sidebar',
                'demo',
                'coreModule',
                'angular-ptbr',
                'fileSaver',
                'oidc'
            ]
        },

        // -- angular modules --
        'commonModule': {
            deps: commonDeps
        },

        // -- template --
        'metronic': {
            deps: [
                'jquery.slimscroll',
                'bootstrap-hover-dropdown',
                'jquery.blockui'
            ]
        },
        'layout': { deps: ['metronic'] },
        'quick-sidebar': { deps: ['metronic'] },
        'demo': { deps: ['layout'] },

        // -- libs --
        'jquery.datatables': { deps: ['jquery'] },
        'datatables.bootstrap': { deps: ['jquery.datatables'] },
        'jquery-migrate': { deps: ['jquery'] },
        'jquery-ui': { deps: ['jquery'] },
        'jquery.blockui': { deps: ['jquery'] },
        'jquery.cokie': { deps: ['jquery'] },
        'jquery.uniform': { deps: ['jquery'] },
        'jquery.slimscroll': { deps: ['jquery'] }, // * barra de rolagem "fininha"
        'bootstrap': { deps: ['jquery', 'jquery-ui'] }, // * recursos visuais do bootstrap
        'bootstrap-switch': { deps: ['jquery'] }, // * chave liga/desliga
        'select2': { deps: ['jquery'] }, // * dropdown sofisticado
        'select2_ptBR': { deps: ['select2'] }, // * traducoes do select2
        'bootstrap-select': { deps: ['select2_ptBR'] }, // * select2 no bootstrap

        // * dropdown nos alertas, nome do usuário, etc
        'bootstrap-hover-dropdown': { deps: ['bootstrap'] },
        'bootstrap-datepicker': { deps: ['bootstrap'] },
        'bootstrap-datetimepicker': { deps: ['jquery', 'bootstrap'] },
        'bootstrap-datetimepicker-ptBR': { deps: ['bootstrap-datetimepicker'] },
        'angular': { deps: ['jquery'] },
        'angular-ptbr': { deps: ['angular'] },
        'ngAnimate': { deps: ['angular'] },
        'ui.router': { deps: ['angular'] },
        'ngResource': { deps: ['angular'] },
        'angular-loading-bar': { deps: ['angular'] },
        'LocalStorageModule': { deps: ['angular'] },
        'ngSanitize': { deps: ['angular'] },
        'angular-ui-select': { deps: ['angular'] },
        'datatables': { deps: ['datatables.bootstrap', 'angular'] },
        'ngNotify': { deps: ['angular'] },
        'angular-input-masks': { deps: ['angular'] },
        'angular-br-filters': { deps: ['angular'] },
        'oidc': { deps: ['jquery'] },
        'angular-ui-mask': { deps: ['angular'] }
    };

    // carrega definições de dependências dos plugins 
    // usados pela aplicação cliente
    for (var k = 0; k < appPlugins.length; k++) {
        var plugin = appPlugins[k];

        if (plugin.deps) {
            coreShim[plugin.name] = { deps: plugin.deps };
        }
    }

    return {
        paths: corePaths,
        shim: coreShim
    }
});