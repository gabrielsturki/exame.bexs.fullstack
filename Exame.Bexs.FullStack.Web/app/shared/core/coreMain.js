﻿define(['app/custom/config/appModules.js', 'app/custom/config/appConfig.js'], configurarDependencias);

function configurarDependencias(modules, appConfig) {
    var coreDeps = [];
    var modulesPaths = {};
    var modulesShim = {};

    // passa por cada modulo configurado e adiciona nos arrays
    for (var i = 0; i < modules.length; i++) {
        var moduleName = modules[i] + 'Module';

        modulesPaths[moduleName] = 'app/modules/' + modules[i] + '/' + moduleName;
        modulesShim[moduleName] = { deps: ['commonModule'] };
        coreDeps.push(moduleName);
    }

    // adiciona o 'coreModule' ao array (esse módulo é fixo)
    modulesShim['coreModule'] = { deps: coreDeps }

    // prepara objeto de configuracao
    var configRequire = {
        baseUrl: '',
        waitSeconds: 30,
        paths: modulesPaths,
        shim: modulesShim
    };

    if (appConfig.modoDebug == false) {
        configRequire.urlArgs = "mc=" + Date.now().toString();
    }

    // executa a configuração dinâmica
    requirejs.config(configRequire);

    require(['app/shared/core/corePlugins.js'], configuarDependenciasFixas);
}

function configuarDependenciasFixas(corePlugins) {
    require.config({
        baseUrl: "",
        paths: corePlugins.paths,
        shim: corePlugins.shim
    });

    require(['app'], inicializarApp);
}

function inicializarApp() {
    console.info('(Core) Todas as dependências foram carregadas..');

    angular.bootstrap(document, ['core']);
}