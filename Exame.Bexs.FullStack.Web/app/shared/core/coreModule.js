﻿define(function (require) {
    var appConfig = require('app/custom/config/appConfig.js');

    var app = angular.module('core', require('app/custom/config/appModules.js'))
        .controller('bodyController', require('app/shared/core/controllers/bodyController.js'))
        .controller('headerController', require('app/shared/core/controllers/headerController.js'))
        .controller('notificationController', require('app/shared/core/controllers/notificationController.js'))
        .controller('inboxController', require('app/shared/core/controllers/inboxController.js'))
        .controller('todoController', require('app/shared/core/controllers/todoController.js'))
        .controller('sideBarController', require('app/shared/core/controllers/sideBarController.js'))
        .controller('customizerController', require('app/shared/core/controllers/customizerController.js'))
        .controller('quickSidebarController', require('app/shared/core/controllers/quickSidebarController.js'))
        .controller('footerController', require('app/shared/core/controllers/footerController.js'))
        .controller('homeController', require('app/shared/core/controllers/homeController.js'))
        .config(configurarModulo)
        .run(executarModulo);

    // ----------------------------------
    // Configurações iniciais
    // ----------------------------------
    function configurarModulo($urlRouterProvider, $stateProvider) {
        var mc = '';

        if (appConfig.modoDebug == false) {
            mc = '?mc=' + Date.now().toString()
        }

        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: 'app/shared/core/views/home.html' + mc,
                controller: 'homeController'
            });

        if (appConfig.rotaInicial) {
            $urlRouterProvider.otherwise(appConfig.rotaInicial);
        } else {
            $urlRouterProvider.otherwise("/");
        }
    }

    // ----------------------------------
    // Startup
    // ----------------------------------
    function executarModulo() {
        document.title = appConfig.titulo || "Exame Banco Bexs";
    }

    return app;
});