﻿define(function () {
    var _template = '<div class="alert alert-block alert-danger">' +
                    '    <button type="button" class="close" ng-click="$ctrl.close()"></button>' +
                    '    <h4 class="alert-heading">Os seguintes erros foram encontrados!</h4>' +
                    '    <ul><li ng-repeat="error in $ctrl.errors">{{ error }}</li></ul>' +
                    '</div>';

    function _controller($rootScope, $element, $timeout) {
        var vm = this;
        var timer;

        this.$onInit = function () {
            $element.hide();
            vm.errors = [];
        };

        this.$onDestroy = function () {
            $timeout.cancel(timer);
        };

        this.close = function () {
            $element.fadeOut(function () {
                vm.errors = [];
            });
        };

        $rootScope.$on("responseError", function (event, data) {
            vm.errors = data;

            $timeout.cancel(timer);
            $element.fadeIn();

            timer = $timeout(function () {
                $element.fadeOut();
            }, 10000);
        });
    };

    return {
        template: _template,
        controller: _controller
    };
});