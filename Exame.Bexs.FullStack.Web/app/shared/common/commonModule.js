﻿define(function (required) {
    var carregarOutrasConfiguracoes = function (appPlugins) {
        for (var i = 0; i < appPlugins.length; i++) {
            var item = appPlugins[i];
            if (item.config) {
                require([item.config], incluirProviderConfig);
            }
        }
    };

    var carregarOutrosPlugins = function (customPlugins) {
        customPlugins.directives.forEach(carregarDiretiva);
        customPlugins.services.forEach(carregarServico);
    };

    var carregarDiretiva = function (item) {
        require([item.path], function (diretiva) {
            app.directive(item.name, diretiva);
        });
    };

    var carregarServico = function (item) {
        require([item.path], function (servico) {
            app.factory(item.name, servico);
        });
    };

    var incluirProviderConfig = function (provider) {
        app.config(provider);
    };

    var moduleDeps = [
        'ngSanitize',
        'ngResource',
        'ngAnimate',
        'ngNotify',
        'ui.router',
        'datatables',
        'angular-loading-bar',
        'LocalStorageModule',
        'ui.utils.masks',
        'idf.br-filters'
    ];

    var appPlugins = require('app/custom/config/appPlugins.js');

    for (var i = 0; i < appPlugins.length; i++) {
        var plugin = appPlugins[i];

        if (plugin.commonDep) {
            moduleDeps.push(plugin.name);
        }
    }

    var app = angular.module('common', moduleDeps)
        .factory('coreService', require('app/shared/common/services/coreService.js'))
        .factory('errorsValidationService', require('app/shared/common/services/errorsValidationService.js'))

    // -- carrega as diretivas da aplicação --
    require(['app/custom/config/appCustomPlugins.js'], carregarOutrosPlugins);

    // -- carrega as configurações comuns --
    app.config(configurarLoadingBar);

    app.config(configurarResource);

    // -- carrega as configurações da aplicação --
    require(['app/custom/config/appPlugins.js'], carregarOutrasConfiguracoes);

    // -- execução das rotinas iniciais --
    app.run(executarModulo);

    // ----------------------------------
    // Startup
    // ----------------------------------
    function configurarLoadingBar(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }

    function configurarResource($resourceProvider) {
        $resourceProvider.defaults.actions = {
            delete: { method: 'DELETE' },
            get: { method: 'GET' },
            getDetails: { method: 'GET', params: { action: 'detalhes' } },
            query: { method: 'GET', isArray: true },
            remove: { method: 'DELETE' },
            save: { method: 'POST' },
            update: { method: 'PUT' }
        };
    }

    function executarModulo(ngNotify) {
        // configura os valores para plugin de notificação
        ngNotify.config({
            theme: 'pure',      // pure (default) | prime | pastel | pitchy
            position: 'bottom', // bottom (default) | top
            duration: 4000,     // milisegundos
            type: 'info',       // info (default) | error | success | warn | grimace
            sticky: false,      // true | false : indica se a mensagem fica congelada ou some passada a duração
            button: true,       // true | false : indica se exibe o botão pra fechar a notificação
            html: false         // true | false : indica se permite a exibição de conteúdo html
        });
    }

    return app;
});