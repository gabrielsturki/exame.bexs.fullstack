﻿define(function() {
    var coreService = function ($q) {
        var factory = {};

        factory.toCamelCase = function (str) {
            return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
                if (+match === 0) return "";
                return index == 0 ? match.toLowerCase() : match.toUpperCase();
            });
        };

        factory.loadController = function (module, controller) {
            var controllerPath = module.paths.controllers() + controller + '.js';

            var defered = $q.defer();
            require([controllerPath], function () {
                defered.resolve();
            });

            return defered.promise;
        };

        factory.parseErrors = function (response) {
            var errors = [];

            var data = response.data;

            if (data.modelState) {
                for (var key in data.modelState) {
                    for (var i = 0; i < data.modelState[key].length; i++) {
                        errors.push(data.modelState[key][i]);
                    }
                }
            } else {
                if (data.exceptionMessage) {
                    errors.push(data.exceptionMessage);
                } else {
                    if (data.message) {
                        errors.push(data.message);
                    }
                }
            }

            return errors;
        };

        return factory;
    };

    return coreService;
});