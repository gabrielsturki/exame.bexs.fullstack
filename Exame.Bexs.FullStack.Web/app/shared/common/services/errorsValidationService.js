﻿define(function () {
    var errorsValidationService = function () {
        var errorsValidation = {}

        errorsValidation.parseErrors = function (response) {
            var errors = [];

            if (!response) {
                return;
            }

            if (response.modelState) {
                for (var key in response.modelState) {
                    for (var i = 0; i < response.modelState[key].length; i++) {
                        errors.push(response.modelState[key][i]);
                    }
                }
            } else if (response.errors) {
                for (var key in response.errors) {
                    errors.push(response.errors[key]);
                }
            } else {
                if (response.message) {
                    errors.push(response.message);
                } else {
                    errors.push("Erro não esperado, por favor contate o administrador do sistema.");
                }
            }

            return errors;
        }

        return errorsValidation;
    }

    return errorsValidationService;
});