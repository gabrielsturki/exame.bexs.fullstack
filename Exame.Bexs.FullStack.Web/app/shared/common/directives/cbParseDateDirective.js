﻿define(function () {
    var cbParseDate = function ($filter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var formatTo = "dd/MM/yyyy";

                var parseDate = function (str) {
                    var dateParts = str.split("/");

                    if (dateParts.length !== 3)
                        return null;

                    var year = dateParts[2];
                    var month = dateParts[1];
                    var day = dateParts[0];

                    if (isNaN(day) || isNaN(month) || isNaN(year))
                        return null;

                    var result = new Date(year, (month - 1), day);

                    if (result.getDate() != day)
                        return null;

                    if (result.getMonth() != (month - 1))
                        return null;

                    if (result.getFullYear() != year)
                        return null;

                    return result;
                }

                ngModel.$formatters.push(function (value) {
                    var formatedDate = $filter('date')(value, formatTo);

                    return formatedDate;
                });

                ngModel.$parsers.push(function (value) {
                    var rawDate = parseDate(value);

                    // todo: avaliar se campo está válido ou não
                    // considerando se o mesmo é obrigatório
                    // var isValidDate = angular.isDate(rawDate);
                    // ngModel.$setValidity('is_valid', isValidDate);

                    return rawDate;
                });
            }
        };
    };

    return cbParseDate;
});