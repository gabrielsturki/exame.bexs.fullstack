﻿define(function () {
    var cbBlockUi = function ($http) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.ajaxCalls = function () {
                    return $http.pendingRequests.length;
                };

                var firstLoad = true;

                scope.$watch(scope.ajaxCalls, function (newValue, oldValue) {
                    if (newValue > 0 && (oldValue == 0 || firstLoad == true)) {
                        firstLoad = false
                        Metronic.blockUI({ animate: true });
                    }
                    else if (newValue == 0 && oldValue != 0) {
                        Metronic.unblockUI();
                    }
                });
            }
        };
    };
    return cbBlockUi;
});