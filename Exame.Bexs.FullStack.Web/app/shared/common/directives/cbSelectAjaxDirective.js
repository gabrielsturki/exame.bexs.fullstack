﻿define(function () {
    var cbSelectAjax = function ($http) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                ngModel: '=',
                initValue: '='
            },
            link: function (scope, element, attrs, ngModel) {
                var placeholder = attrs.placeholder || "Selecione um valor..";
                var idField = attrs.fieldId || 'id';
                var textField = attrs.fieldText || 'text';
                var minimumInput = tratarInputMinimo(attrs.minimumInput);

                // se a url nao for informada.. nada pode ser feito
                if (!attrs.apiUrl) {
                    return;
                }

                var apiUrl = attrs.apiUrl;

                var textSearch = function (item) {
                    return item[textField];
                };

                var formatSelection = function (item) {
                    return item[textField];
                };

                var formatResult = function (item) {
                    return item[textField];
                };

                var configurarControle = function (response) {
                    var obterTransporte = function (queryParams) {
                        var urlPesquisa = "";

                        if (queryParams.url.indexOf("?") != -1) {
                            urlPesquisa = queryParams.url + '&' + $.param(queryParams.data);
                        }
                        else {
                            urlPesquisa = queryParams.url + '?' + $.param(queryParams.data);
                        };

                        return $http
                            .get(urlPesquisa)
                            .success(queryParams.success);
                    };

                    var configuraDados = function (term, page) {
                        var params = {};
                        params[textField] = term;
                        return params;
                    };

                    var selectInstance = element.select2({
                        allowClear: true,
                        placeholder: placeholder,
                        minimumInputLength: minimumInput,
                        ajax: {
                            url: apiUrl,
                            transport: obterTransporte,
                            dataType: 'json',
                            quietMillis: 500,
                            data: configuraDados,
                            results: function (data, page) {
                                return { results: data };
                            },
                            cache: false
                        },
                        id: idField,
                        formatResult: formatResult,
                        formatSelection: formatSelection,
                        initSelection: function (element, callback) {
                            scope.$watch(function () {
                                return scope.initValue;
                            }, function () {
                                if (!scope.initValue)
                                    return;

                                callback(scope.initValue);
                            });
                        },
                    }).select2('val', []);

                    // observa as alteracoes no ngModel
                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, function (newValue) {
                        if (!newValue) {
                            selectInstance.select2('val', null);
                        }
                    });

                    return;
                };

                var mapearEventos = function (controle) {
                    // select
                    element.on("change", function (e) {
                        ngModel.$setViewValue(e.val);
                    });

                    // removed
                    element.on("select2-removed", function (e) {
                        ngModel.$setViewValue(null);
                    });
                };

                function tratarInputMinimo(minimumInput) {
                    if (!minimumInput || isNaN(minimumInput)) return 3;

                    return minimumInput;
                }

                attrs.$observe("apiUrl", function () {
                    apiUrl = attrs.apiUrl;
                    configurarControle();
                });

                // Inicializa o controle
                configurarControle();
                mapearEventos();
            }
        }
    };

    return cbSelectAjax;
});