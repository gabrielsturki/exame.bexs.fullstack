﻿define(function () {
    var cbValidationData = function ($filter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            /* 
            tipo: Determina o tipo de validação que será dada ao campo.
            output1: Campo opcional. Se for necessária uma saída de dados alternativa, por exemplo, sem a formatação, pode ser
                     guardado aqui.
            param1: parametro opcional para configuração da diretiva.
            */
            scope: {
                model: '=ngModel',
                tipo: '@',
                output1: '=',
                param1: '@'
            },
            link: function (scope, elem, attr, ctrl) {

                // Filtro apenas para números (inclui negativos), para campos input. Saída de dados é uma string. Pode fixar numero de casas decimais usando "param1".
                var onlyNumber = function () {
                    scope.$watch('model', function () {
                        if (scope.model) {
                            if (scope.model.toString().charAt(0) == "-") {
                                scope.model = "-" + scope.model.substr(1, scope.model.length).toString().trim().replace(/[^0-9.]/g, '').replace(/[.]/g, ',');
                            } else {
                                scope.model = scope.model.toString().trim().replace(/[^0-9.,]/g, '').replace(/[.]/g, ',');
                            }

                            if (scope.param1) {
                                var aux = parseFloat(scope.model.replace(/[,]/g, '.')).toFixed(scope.param1);
                                scope.model = aux.toString().trim().replace(/[.]/g, ',');
                            }
                        }
                    })
                }

                // Filtro apenas para números (inclui negativos), para campos input. Saída de dados é uma string. Pode fixar numero de casas decimais usando "param1".
                var onlyNumberBeta = function () {
                    ctrl.$parsers.push(function (value) {
                        var valorView = value
                        if (value || value == 0) {
                            if (value.toString().charAt(0) == "-") {
                                value = "-" + value.toString().substr(1, value.length).toString().trim().replace(/[^0-9.,]/g, '').replace(/[.]/g, ',');
                            } else {
                                value = value.toString().trim().replace(/[^0-9,]/g, '');
                            }

                            valorView = value;

                            value = parseFloat(value.replace(/[,]/g, '.'))

                            if (scope.param1) {
                                value = value.toFixed(scope.param1);
                            }

                            ctrl.$setViewValue(valorView);
                            ctrl.$render();

                            return value;
                        }
                    })

                    ctrl.$formatters.push(function (value) {
                        if (value || value == 0) {
                            if (value.toString().charAt(0) == "-") {
                                value = "-" + value.toString().substr(1, value.length).toString().trim().replace(/[^0-9.,]/g, '').replace(/[.]/g, ',');
                            } else {
                                value = value.toString().trim().replace(/[^0-9.,]/g, '').replace(/[.]/g, ',');
                            }

                            if (scope.param1) {
                                var aux = parseFloat(value.replace(/[,]/g, '.')).toFixed(scope.param1);
                                value = aux.toString().trim().replace(/[.]/g, ',');
                                ctrl.$setViewValue(value);
                                ctrl.$render();
                            }

                            return value;
                        }
                    })
                }

                // Máscara para CPF. Formatará o ngModel com "." e "/".
                // Permite também uma saída de dados alternativa sem formatação. A variável da saída alternativa
                // deve ser inicializada como string vazia ("").
                // scope.model : Entrada principal de dados, o campo Input.
                var cpfFormat = function () {
                    scope.$watch('model', function () {
                        if (scope.model) {
                            var cpfFormatado = scope.model.toString().trim().replace(/[^0-9]/g, '');
                            switch (cpfFormatado.length) {
                                case 0: case 1: case 2: case 3:
                                    break;
                                case 4: case 5: case 6:
                                    cpfFormatado = cpfFormatado.slice(0, 3) + "." + cpfFormatado.slice(3, cpfFormatado.length);
                                    break;
                                case 7: case 8: case 9:
                                    cpfFormatado = cpfFormatado.slice(0, 3) + "." + cpfFormatado.slice(3, 6) + "." + cpfFormatado.slice(6, cpfFormatado.length);
                                    break;
                                case 10: case 11:
                                    cpfFormatado = cpfFormatado.slice(0, 3) + "." + cpfFormatado.slice(3, 6) + "." + cpfFormatado.slice(6, 9) + "/" + cpfFormatado.slice(9, cpfFormatado.length);
                                    break
                                default:
                                    cpfFormatado = cpfFormatado.slice(0, 3) + "." + cpfFormatado.slice(3, 6) + "." + cpfFormatado.slice(6, 9) + "/" + cpfFormatado.slice(9, 11);
                            }
                            scope.model = cpfFormatado
                            if (scope.output1 != null) {
                                scope.output1 = cpfFormatado.trim().replace(/[^0-9]/g, '')
                            }
                        }
                    });
                }

                // Máscara para RG. Formatará o ngModel com "." e "-".
                // Permite também uma saída de dados alternativa sem formatação. A variável da saída alternativa
                // deve ser inicializada como string vazia ("").
                // scope.model : Entrada principal de dados, o campo Input.
                var rgFormat = function () {
                    scope.$watch('model', function () {
                        if (scope.model) {
                            var rgFormatado = scope.model.toString().trim().replace(/[^0-9]/g, '');
                            switch (rgFormatado.length) {
                                case 0: case 1: case 2:
                                    break;
                                case 3: case 4: case 5:
                                    rgFormatado = rgFormatado.slice(0, 2) + "." + rgFormatado.slice(2, rgFormatado.length);
                                    break;
                                case 6: case 7: case 8:
                                    rgFormatado = rgFormatado.slice(0, 2) + "." + rgFormatado.slice(2, 5) + "." + rgFormatado.slice(5, rgFormatado.length);
                                    break;
                                case 9:
                                    rgFormatado = rgFormatado.slice(0, 2) + "." + rgFormatado.slice(2, 5) + "." + rgFormatado.slice(5, 8) + "-" + rgFormatado.slice(8, rgFormatado.length);
                                    break
                                default:
                                    rgFormatado = rgFormatado.slice(0, 2) + "." + rgFormatado.slice(2, 5) + "." + rgFormatado.slice(5, 8) + "-" + rgFormatado.slice(8, 9);
                            }
                            scope.model = rgFormatado;
                            if (scope.output1 != null) {
                                scope.output1 = rgFormatado.trim().replace(/[^0-9]/g, '')
                            }
                        }
                    });
                }

                // Máscara para CNPJ. Formatará o ngModel com ".", "/" e "-".
                // Permite também uma saída de dados alternativa sem formatação. A variável da saída alternativa
                // deve ser inicializada como string vazia ("").
                // scope.model : Entrada principal de dados, o campo Input.
                var cnpjFormat = function () {
                    scope.$watch('model', function () {
                        if (scope.model) {
                            var cnpjFormatado = scope.model.toString().trim().replace(/[^0-9]/g, '');
                            switch (cnpjFormatado.length) {
                                case 0: case 1: case 2:
                                    break;
                                case 3: case 4: case 5:
                                    cnpjFormatado = cnpjFormatado.slice(0, 2) + "." + cnpjFormatado.slice(2, cnpjFormatado.length);
                                    break;
                                case 6: case 7: case 8:
                                    cnpjFormatado = cnpjFormatado.slice(0, 2) + "." + cnpjFormatado.slice(2, 5) + "." + cnpjFormatado.slice(5, cnpjFormatado.length);
                                    break;
                                case 9: case 10: case 11: case 12:
                                    cnpjFormatado = cnpjFormatado.slice(0, 2) + "." + cnpjFormatado.slice(2, 5) + "." + cnpjFormatado.slice(5, 8) + "/" + cnpjFormatado.slice(8, cnpjFormatado.length);
                                    break
                                case 13: case 14:
                                    cnpjFormatado = cnpjFormatado.slice(0, 2) + "." + cnpjFormatado.slice(2, 5) + "." + cnpjFormatado.slice(5, 8) + "/" + cnpjFormatado.slice(8, 12) + "-" + cnpjFormatado.slice(12, cnpjFormatado.length);
                                    break
                                default:
                                    cnpjFormatado = cnpjFormatado.slice(0, 2) + "." + cnpjFormatado.slice(2, 5) + "." + cnpjFormatado.slice(5, 8) + "/" + cnpjFormatado.slice(8, 12) + "-" + cnpjFormatado.slice(12, 14);
                            }
                            scope.model = cnpjFormatado;
                            if (scope.output1 != null) {
                                scope.output1 = cnpjFormatado.trim().replace(/[^0-9]/g, '')
                            }
                        }
                    });
                }

                // Formatação para Reais em campos de input. Saída de dados é uma string
                var currencyFormat = function () {
                    if (ctrl) {
                        ctrl.$formatters.unshift(function (a) {
                            return $filter("currency")(ctrl.$modelValue)
                        });

                        //"change" é um evento Angular que dispara quando o elemento é alterado.
                        //Quando ocorrer, apagará os caracteres não permitidos, e então substitir "," por "." para poder usar o $filter
                        elem.bind('change', function (event) {
                            var plainNumber = elem.val().replace(/[^\d|\-+|\,+]/g, '');
                            plainNumber = plainNumber.replace(/,/g, '.')
                            elem.val($filter("currency")(plainNumber));
                        });
                    }
                }

                // Máscara para campos de Hora (tempo). Formatará o ngModel com ":". Saída de dados será uma string.
                // Permite também uma saída de dados com segundos, dada a configuração do campo "param1".
                // scope.model : Entrada principal de dados, o campo Input.
                // scope.param1 : Configuração para ativar os segundos na formatação. True = segundos ativos.
                var timeFormat = function (horaFormatado) {

                    var validaHH = function (hora) {
                        if (hora.slice(0, 1) == 0 || hora.slice(0, 1) == 1) {
                        } else if (hora.slice(0, 1) == 2) {
                            if (hora.slice(1, 2) && (hora.slice(1, 2) >= 4 || hora.slice(1, 2) < 0)) {
                                return hora.slice(0, 1);
                            }
                        } else {
                            return "";
                        }
                        return hora
                    }

                    var validaMM = function (hora) {
                        if (hora.slice(2, 3) > 5 || hora.slice(2, 3) < 0) {
                            return hora.slice(0, 2);
                        }
                        return hora
                    }

                    var validaSS = function (hora) {
                        if (hora.slice(4, 5) > 5 || hora.slice(4, 5) < 0) {
                            return hora.slice(0, 4);
                        }
                        return hora
                    }

                    scope.$watch('model', function () {
                        if (scope.model) {
                            var horaFormatado = scope.model.toString().trim().replace(/[^0-9]/g, '');
                            var validar;
                            switch (horaFormatado.length) {
                                case 0: case 1: case 2:
                                    validar = validaHH(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }
                                    break;
                                case 3: case 4:
                                    validar = validaHH(horaFormatado);
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    validar = validaMM(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    horaFormatado = horaFormatado.slice(0, 2) + ":" + horaFormatado.slice(2, horaFormatado.length);
                                    break;
                                case 5: case 6:

                                    validar = validaHH(horaFormatado);
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    validar = validaMM(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    validar = validaSS(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    if (scope.param1 == "true") {
                                        horaFormatado = horaFormatado.slice(0, 2) + ":" + horaFormatado.slice(2, 4) + ":" + horaFormatado.slice(4, horaFormatado.length);
                                    } else {
                                        horaFormatado = horaFormatado.slice(0, 2) + ":" + horaFormatado.slice(2, 4)
                                    }
                                    break;
                                default:

                                    validar = validaHH(horaFormatado);
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    validar = validaMM(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }

                                    validar = validaSS(horaFormatado)
                                    if (horaFormatado != validar) {
                                        horaFormatado = validar;
                                        break;
                                    }
                                    if (scope.param1 == "true") {
                                        horaFormatado = horaFormatado.slice(0, 2) + ":" + horaFormatado.slice(2, 4) + ":" + horaFormatado.slice(4, 6);
                                    } else {
                                        horaFormatado = horaFormatado.slice(0, 2) + ":" + horaFormatado.slice(2, 4)
                                    }
                                    break
                            }
                            scope.model = horaFormatado;
                        }
                    });
                }

                switch (scope.tipo) {
                    case "onlyNumber":
                        onlyNumber();
                        break;
                    case "onlyNumberBeta":
                        onlyNumberBeta();
                        break;
                    case "cpfFormat":
                        cpfFormat();
                        break;
                    case "rgFormat":
                        rgFormat();
                        break;
                    case "cnpjFormat":
                        cnpjFormat();
                        break;
                    case "currencyFormat":
                        currencyFormat();
                        break;
                    case "timeFormat":
                        timeFormat();
                        break;
                }
            }
        };
    };

    return cbValidationData;
});