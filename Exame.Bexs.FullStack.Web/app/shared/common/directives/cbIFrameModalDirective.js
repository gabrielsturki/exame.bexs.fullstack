﻿define(function () {
    /*
     *  @- Diretiva utilizada para suprir a necessidade de sistemas que necessitam de telas/funcionalidades
     * que estão alocados em outros sistemas...
     *
     *  @- Tendo em vista que a tela será carregada dentro de um modal via i-frame, é importante que a tela
     * esteja adaptada para o mesmo.
     *
     *  @- Funcionamento:
     *
     *  <cb-iframe-modal iframe-url=// obtida através de "appInfoService.obterUrlAplicacao"
     *                   iframe-id=//id do modal && chave que será usada para comunicação entre controler <=> diretiva
     *                   size=// modal-lg, modal-full, modal-sm ou null
     *                   title=// titulo do modal>
     *  </cb-iframe-modal>
     *
     *  Para estabelecer uma conexão entre a diretiva e a página, é necessário que no controller.js da página
     * que está sendo chamada, exista a seguinte estrutura:
     * 
     *                 var eventMethod = window.addEventListener
     *                     ? "addEventListener"
     *                     : "attachEvent";
     *
     *              var eventer = window[eventMethod];
     *
     *              var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";
     *
     *              eventer(messageEvent, function (e) {
     *                  if (e.data.substring(4,0) == "link") {
     *                      enderecoPost = e.data.substring(4, 300);
     *
     *                      parent.postMessage({
     *                          id: // o valor deve ser o mesmo colocado na chamada da diretiva em 'iframe-id',
     *                          dados: "Status_Ok"
     *                      }, enderecoPost);
     *                  }
     *              });
     *
     *                $scope.fecharIframe = function () {
     *                   parent.postMessage({
     *                       id: // o valor deve ser o mesmo colocado na chamada da diretiva em 'iframe-id',
     *                       dados: "fechaModal"
     *                   }, enderecoPost)
     *               };
     *
     *  @- Tendo a conexão estabelecida, a diretiva receberá os dados enviados pelo controller.js e irá transmiti-lá;
     *  
     *  Para recebe-lá é necessário que no controller.js de quem chama a diretiva, exista a seguinta estrutura:
     *             $scope.$on('mensagemIframe', function (event, data) {
     *               // {{seu codigo}}
     *           })
     *
     *  @- Estrutura para que a diretiva entenda os dados que irá receber e passe para frente =>
     *      {id: // o valor deve ser o mesmo colocado na chamada da diretiva,
     *       dados: ...
     *      }   
     *
     */

    var cbIFrameModal = function ($http) {
        return {
            restrict: 'E',
            scope: {
                iframeId: '@',
                size: '@',
                title: '@',
                iframeUrl: '='
            },

            templateUrl: 'app/custom/views/cb-iFrame-modal.html',
            link: function (scope, element, attrs) {
                scope.$watch('iframeUrl', function (newValue, oldValue) {
                    if (newValue) {
                        configurarDiretiva();
                    }
                });

                function configurarDiretiva() {
                    $("#" + scope.iframeId + "iFrameModal").attr('src', scope.iframeUrl);

                    var iFrame = document.getElementById(scope.iframeId + "iFrameModal");
                    var linkParent = window.location.href;

                    var calcHeight = function () {
                        $("#" + scope.iframeId + "iFrameModal").height($(window).height() - 120);
                    }

                    $(document).ready(function () {
                        calcHeight();
                    });

                    $("#" + scope.iframeId + "iFrameModal").resize(function () {
                        calcHeight();
                    }).load(function () {
                        calcHeight();
                    });

                    $(window).resize(function () {
                        calcHeight();
                    }).load(function () {
                        calcHeight();
                    });

                    function intervalSet() {
                        if (iFrame.contentWindow) {
                            iFrame.contentWindow.postMessage("link" + linkParent, scope.iframeUrl);
                        }
                    };

                    var sendLinkMessage = setInterval(intervalSet, 300);

                    var eventMethod = window.addEventListener
                        ? "addEventListener"
                        : "attachEvent";

                    var eventer = window[eventMethod];

                    var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

                    eventer(messageEvent, function (e) {
                        if (e.data.id != scope.iframeId)
                            return;

                        if (e.data.dados === "Status_Ok") {
                            clearInterval(sendLinkMessage);
                        }

                        if (e.data.dados === "fechaModal") {
                            $('#' + scope.iframeId).modal("hide");
                        }

                        if (e.data.dados.key === "sendLink") {
                            sendLinkMessage = setInterval(intervalSet, 300);
                        }

                        else {
                            scope.$emit('mensagemIframe', e.data.dados);
                        }
                    });
                }
            }
        };
    };
    return cbIFrameModal;
});