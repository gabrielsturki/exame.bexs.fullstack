﻿define(function () {
    var cbConfirmationModal = function () {
        return {
            restrict: 'E',
            scope: {
                handler: '@',
                title: '@',
                onConfirmClick: '&confirmClick'
            },
            templateUrl: 'app/shared/common/views/cb-confirmation-modal.html',
            replace: true,
            transclude: true,
            link: function (scope, element, attrs) {
                var confirmed = false;

                scope.confirmClick = function (option) {
                    confirmed = option;
                    element.modal('hide');
                };

                element.on('hidden.bs.modal', function (e) {
                    if (confirmed) {
                        scope.onConfirmClick.apply();
                    }
                });

                element.on('shown.bs.modal', function (e) {
                    confirmed = false;
                });
            }
        };
    };

    return cbConfirmationModal;
});