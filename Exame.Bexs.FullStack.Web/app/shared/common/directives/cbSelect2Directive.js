define(function () {
    var cbSelect2 = function ($http) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                ngModel: '=',
                listaOptions: '=?',
                listaPreDefinida: '=?'
            },
            link: function (scope, element, attrs, ngModel) {
                var placeholder = attrs.placeholder || "Selecione um valor..";
                var idField = attrs.fieldId || 'id';
                var textField = attrs.fieldText || 'text';

                // se a url nao for informada e não houver uma lista pré definida, nada pode ser feito
                if (!attrs.apiUrl && typeof (scope.listaPreDefinida) == "undefined") {
                    return;
                }

                var apiUrl = attrs.apiUrl;

                var textSearch = function (item) {
                    return item[textField];
                };

                var formatSelection = function (item) {
                    return item[textField];
                };

                var formatResult = function (item) {
                    return item[textField];
                };

                var getId = function (item) {
                    var items = idField.split(',');

                    if (items.length == 1) return item[idField];

                    // chave composta
                    if (!item) return undefined;

                    var id = "";
                    var separator = "";

                    for (var i = 0; i < items.length; i++) {
                        var field = items[i].trim();

                        id = id + separator + item[field];
                        separator = "_";
                    }

                    return id;
                };

                var getModelValue = function (item) {
                    var items = idField.split(',');

                    if (items.length == 1) return item;
                    if (!item) return undefined;

                    // chave composta
                    var id = "";
                    var separator = "";

                    for (var i = 0; i < items.length; i++) {
                        var field = items[i].trim();

                        id = id + separator + item[field];
                        separator = "_";
                    }

                    return id;
                };

                var configurarControle = function (response) {

                    // após o retorno dos dados, configura o controle
                    var selectInstance = element.select2({
                        allowClear: true,
                        placeholder: placeholder,
                        id: getId,
                        data: { results: response.data, text: textField },
                        formatResult: formatResult,
                        formatSelection: formatSelection
                    });

                    // observa as alteracoes no ngModel
                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, function () {
                        var modelValue = getModelValue(ngModel.$modelValue);
                        selectInstance.select2('val', modelValue);
                    });

                    if (scope.listaOptions) {
                        scope.listaOptions = response.data;
                    }

                    return;
                };

                var mapearEventos = function () {
                    // select
                    element.on("change", function (e) {
                        var items = idField.split(',');

                        var viewValue = e.val;

                        if (items.length > 1) {
                            viewValue = {};

                            for (var i = 0; i < items.length; i++) {
                                var field = items[i].trim();

                                if (e.added) {
                                    viewValue[field] = e.added[field];
                                }
                            }
                        }

                        ngModel.$setViewValue(viewValue);
                    });

                    // removed
                    element.on("select2-removed", function () {
                        ngModel.$setViewValue(null);
                    });
                };

                if (typeof (scope.listaPreDefinida) != "undefined") {
                    scope.$watch('listaPreDefinida', function (newValue, oldValue) {
                        if (newValue) {
                            var lista = { data: scope.listaPreDefinida };
                            configurarControle(lista);
                            mapearEventos();
                        }
                    })
                }
                else {
                    var apiUrl = attrs.apiUrl;

                    attrs.$observe("apiUrl", function () {
                        apiUrl = attrs.apiUrl;

                        $http.get(apiUrl)
                        .then(configurarControle)
                        .then(mapearEventos);
                    });
                }
            }
        }
    };

    return cbSelect2;

});