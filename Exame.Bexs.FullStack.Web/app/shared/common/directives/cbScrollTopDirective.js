﻿define(function () {
    var cbScrollTop = function ($rootScope, $window) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    if (toParams.blockScrollTop) return;

                    var y = 0;

                    if (toParams.scrollToY) {
                        y = toParams.scrollToY
                    }

                    // scroll x/y
                    $window.scrollTo(0, y);
                });
            }
        };
    };
    return cbScrollTop;
});