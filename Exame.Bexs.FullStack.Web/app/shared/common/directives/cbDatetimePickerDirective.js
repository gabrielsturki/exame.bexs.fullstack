define(function (require) {

    var moment = require('assets/plugins/moment/moment-with-locales.min');
    var cbDatetimePicker = function ($filter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                // --------------------------------------
                // Formatos
                // --------------------------------------
                var isoFormat = "YYYY-MM-DDTHH:mm:ss";
                var angularFormat = attrs.cbDatetimePicker;

                if (!angularFormat) {
                    angularFormat = converterParaAngular(attrs.dateFormat || 'dd/mm/yyyy');
                }

                var datePickerFormat = converterParaDatetimePicker(angularFormat);

                var momentFormat = converterParaMoment(angularFormat);

                // --------------------------------------
                // Configuração do DatetimePicker
                // --------------------------------------
                var minView = attrs.minView || obterVisaoMinima(angularFormat);
                var maxView = attrs.maxView || obterVisaoMaxima(angularFormat);
                var startView = attrs.startView || obterVisaoInicial(angularFormat);
                var showTodayBtn = attrs.todayBtn === 'true';
                var showTodayHighlight = attrs.todayHighlight === 'true';
                var datePickerPosition = obterPosicaoElemento(attrs.position)

                var options = {
                    format: datePickerFormat,
                    autoclose: true,
                    language: "pt-BR",
                    pickerPosition: datePickerPosition,
                    minView: minView,
                    maxView: maxView,
                    startView: startView,
                    todayBtn: showTodayBtn,
                    todayHighlight: showTodayHighlight
                };

                // --------------------------------------
                // Inicializar DatetimePicker
                // --------------------------------------
                element.datetimepicker(options);

                // --------------------------------------
                // Parses e Formatadores (AngularJs)
                // --------------------------------------
                ngModel.$formatters.push(function (value) {
                    if (!value) return;

                    moment.locale('pt-BR');

                    return moment.utc(value, isoFormat)
                        .format(momentFormat);
                });

                ngModel.$parsers.push(function (value) {
                    if (!value) return null;

                    moment.locale('pt-BR');

                    var rawDate = moment.utc(value, momentFormat)
                        .format(isoFormat);

                    // todo: avaliar se campo está válido ou não
                    // considerando se o mesmo é obrigatório
                    // var isValidDate = angular.isDate(rawDate);
                    // ngModel.$setValidity('is_valid', isValidDate);

                    return rawDate;
                });

                // converte o formato dateTimePicker para angular
                function converterParaAngular(formato) {
                    return formato
                        .replace(/m/g, "M")
                        .replace(/h/g, "H")
                        .replace(/i/g, "m");
                }

                function converterParaDatetimePicker(formato) {
                    // algumas substituições não podem ser feitas 
                    // diretamente, portanto é necessário uma 
                    // substituição intermediária, aqui chamada
                    // de 'passagem'
                    return formato
                        .replace(/m/g, "i")
                        .replace(/H/g, "x")     // passagem --> h
                        .replace(/h/g, "H")
                        .replace(/x/g, "h")
                        .replace(/MMMM/g, "XX") // passagem --> MM
                        .replace(/MMM/g, "Y")   // passagem --> M
                        .replace(/MM/g, "mm")
                        .replace(/M/g, "m")
                        .replace(/XX/g, "MM")
                        .replace(/Y/g, "M");
                }

                function converterParaMoment(formato) {
                    return formato
                        .replace(/d/g, "D")
                        .replace(/y/g, "Y");
                }

                function obterVisaoMinima(formato) {
                    if (formato.indexOf("m") >= 0) return 'hour';
                    if (formato.indexOf("H") >= 0) return 'day';
                    if (formato.indexOf("d") >= 0) return 'month';
                    if (formato.indexOf("M") >= 0) return 'year';
                    if (formato.indexOf("y") >= 0) return 'decade';

                    return 'hour';
                }

                function obterVisaoMaxima(formato) {
                    if (formato.indexOf("y") >= 0) return 'decade';
                    if (formato.indexOf("M") >= 0) return 'year';
                    if (formato.indexOf("d") >= 0) return 'month';
                    if (formato.indexOf("H") >= 0) return 'day';
                    if (formato.indexOf("m") >= 0) return 'hour';

                    return 'decade';
                }

                function obterVisaoInicial(formato) {
                    if (formato.indexOf("d") >= 0) return 'month';
                    if (formato.indexOf("M") >= 0) return 'year';
                    if (formato.indexOf("y") >= 0) return 'decade';
                    if (formato.indexOf("H") >= 0) return 'day';
                    if (formato.indexOf("m") >= 0) return 'day';

                    return 'hour';
                }

                function obterPosicaoElemento(position) {
                    if (!position) {
                        return "top-right";
                    }

                    if (position !== "top-right" &&
                        position !== "top-left" &&
                        position !== "bottom-right" &&
                        position !== "bottom-left") {
                        return "top-right";
                    }

                    return position;
                }
            }
        };
    };

    return cbDatetimePicker;
});