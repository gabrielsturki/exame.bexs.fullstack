﻿define(function () {
    var cbTooltip = function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.tooltip({ trigger: 'hover' });
                element.on('click', function () {
                    element.tooltip('hide');
                });
            }
        };
    };
    return cbTooltip;
});