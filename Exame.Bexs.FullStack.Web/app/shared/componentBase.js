﻿// define uma classe base para criação de componentes
define(function (require) {

    var $ = require('jquery');
    var appConfig = require('app/custom/config/appConfig.js')

    // encapsulated in a Module Class / Function
    // to enable instantiation
    var ComponentBase = function (options) {
        var _options = {};

        var _defaultStates = [
            { name: '', abstract: true, controller: null, template: '<div ui-view></div>' },
            { name: 'lista' },
            { name: 'incluir' },
            { name: 'detalhes', params: ':id' },
            { name: 'editar', params: ':id' }
        ];

        if (typeof (options) == 'string') {
            _options.model = options;
        } else {
            _options = options;
        }

        if (!_options.states) {
            _options.states = _defaultStates;
        }

        var toCamelCase = function camelize(str) {
            return str.replace(/\W+(.)/g, function (match, chr) {
                return chr.toUpperCase();
            });
        };

        var toPascalCase = function (str) {
            var txt = toCamelCase(str);
            return txt.charAt(0).toUpperCase() + txt.substr(1);
        };

        var getModelName = function () {
            var txt = _options.model;
            return txt.charAt(0).toLowerCase() + txt.substr(1);
        };

        var getUrl = function (state) {
            if (state.url) {
                return state.url;
            }

            if (!state.name) {
                return '/' + getModelName();
            }

            // aqui precisa pegar a ultima parte da rota..
            var url = state.name.split('.');

            if (state.params) {
                return '/' + url[url.length - 1] + '/' + state.params;
            }

            return '/' + url[url.length - 1];
        };

        var getTemplateUrl = function (state) {
            if (state.templateUrl) return state.templateUrl;

            var baseDir = 'app/modules/' + getModelName() + '/views/';
            var path = baseDir + getModelName();

            if (state.name) {
                path = path + '.' + state.name;
            }

            if (appConfig.modoDebug == false) {
                return path + '.html?mc=' + Date.now().toString();
            }

            return path + '.html';
        };

        var getController = function (stateName) {
            var name = toPascalCase(stateName.name);

            return getModelName() + name + 'Controller';
        };

        var getStateName = function (state) {
            if (state.name) {
                return getModelName() + '.' + state.name;
            }

            return getModelName();
        }

        var loadController = function (controller) {
            var baseDir = 'app/modules/' + getModelName() + '/controllers/';
            var controllerPath = baseDir + controller + '.js';
            var defered = new $.Deferred();

            require([controllerPath], function () {
                defered.resolve();
            });

            return defered.promise();
        };

        var getConfiguredState = function (state) {
            if (!state) return null;

            var obj = {};

            obj.name = getStateName(state);
            obj.abstract = state.abstract || false;
            obj.url = getUrl(state);

            if (state.template) {
                obj.template = state.template;
            }
            else {
                obj.templateUrl = getTemplateUrl(state);
            }

            if (typeof (state.controller) == 'undefined') {
                var controllerName = getController(state);

                obj.controller = controllerName;

                if (state.resolve) {
                    obj.resolve = state.resolve;
                    obj.resolve.controllerFile = function () {
                        return loadController(controllerName);
                    };
                } else {
                    obj.resolve = {
                        controllerFile: function () {
                            return loadController(controllerName);
                        }
                    };
                }
            }

            return obj;
        };

        var getServiceName = function () {
            return _options.model + 'Service';
        };

        // retorna as propriedades e métodos públicos
        return {
            modelName: _options.name,
            name: getModelName(),
            states: _options.states,
            serviceName: getServiceName(),
            configureState: function (item) {
                return getConfiguredState(item);
            }
        };
    }

    return ComponentBase;
});