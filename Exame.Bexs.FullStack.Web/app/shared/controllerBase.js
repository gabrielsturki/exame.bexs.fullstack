﻿define(function() {

    var ControllerBase = function(modelName, controllerName, idField) {

        var toCamelCase = function (str) {
            return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
                if (+match === 0) return "";
                return index == 0 ? match.toLowerCase() : match.toUpperCase();
            });
        };

        var getModuleName = function () {
            return toCamelCase(modelName);
        };

        var getServiceName = function () {
            return modelName + 'Service';
        };

        var getControllerName = function () {
            return toCamelCase(modelName) + controllerName + 'Controller';
        };

        return {
            name: getControllerName(),
            modelName: modelName,
            idField: idField,
            moduleName: getModuleName(),
            serviceName: getServiceName()
        };

    };

    return ControllerBase;

});