﻿define(function () {
    return {
        clientId: '',
        titulo: '',
        rotaInicial: '/questioning/lista',
        urlBase: '/template',
        modoDebug: true,
        menuCustomizado: true
    };
});