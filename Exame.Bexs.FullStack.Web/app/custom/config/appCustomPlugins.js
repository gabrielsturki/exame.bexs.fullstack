﻿define(function () {
    var _components = [
        // { 
        //      name: 'cbComponentName', 
        //      path: '/app/custom/components/{cbComponentName}Component.js' 
        // }
    ];

    var _directives = [
        // { 
        //      name: 'cbDirectiveName', 
        //      path: '/app/custom/directives/{cbDirectiveName}Directive.js' 
        // }
    ];

    var _filters = [
        // { 
        //      name: 'cbFilterName', 
        //      path: '/app/custom/filters/{cbFilterName}Filter.js' 
        // }
    ];

    var _services = [
        // { 
        //      name: 'cbServiceName', 
        //      path: '/app/custom/services/{cbServiceName}Service.js' 
        // }
    ];

    return {
        components: _components,
        directives: _directives,
        filters: _filters,
        services: _services
    }
});