﻿// -------------------------------------------------------------------------------------------
// appPlugins.js
// Este arquivo deve ser usado para registrar todos os plugins
// que serão utilizados na aplicação além daqueles que já fazem
// parte do conjunto de plugins padrão dos sistemas (definidos no template)
// -------------------------------------------------------------------------------------------

/*
    // ---------------------------------------------------------------------------------------
    // Como configurar: Monte um array de objetos conforme 
    // o modelo abaixo, sendo:
    //  - name: nome do plugin, de preferencia, o nome do módulo angular
    //  - deps: dependencia do plugin, angular, jquery, ..
    //  - config: arquivo js contendo a configuração inicial para o plugin (opcional)
    //  - commonDep: indica se o plugin fará parte das depencias do 'commomModule'
    // ---------------------------------------------------------------------------------------
    var plugins = [
        {
            name: 'angular-google-analytics',
            path: 'assets/plugins/angular-google-analytics/angular-google-analytics.min',
            deps: ['angular'],
            config: 'app/custom/providers/googleAnalyticsProvider.js',
            commonDep: true
        }
    ];

*/

define(function () {

    var plugins = [
        //{
        //    name: 'angularFileUpload',
        //    path: 'assets/plugins/angular-file-upload/angular-file-upload.min',
        //    deps: ['angular']
        //},
        //{
        //    name: 'amCharts',
        //    path: 'assets/plugins/angular-amCharts/amcharts',
        //    deps: ['jquery']
        //},
        //{
        //    name: 'amChartsSerial',
        //    path: 'assets/plugins/angular-amCharts/serial',
        //    deps: ['amCharts']
        //},
        //{
        //    name: 'amChartsPie',
        //    path: 'assets/plugins/angular-amCharts/pie',
        //    deps: ['amCharts']
        //},
        //{
        //    name: 'amChartsDirective',
        //    path: 'assets/plugins/angular-amCharts/amChartsDirective',
        //    deps: ['amCharts']
        //},
        //{
        //    name: 'angular-google-analytics',
        //    path: 'assets/plugins/angular-google-analytics/angular-google-analytics.min',
        //    deps: ['angular'],
        //    config: 'app/custom/providers/googleAnalyticsProvider.js',
        //    commonDep: true
        //}

        //{
        //    name: 'angularUiGrid',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.min',
        //    deps: ['angular'],
        //},
        //{
        //    name: 'angularUiGridCore',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.core.min',
        //    deps: ['angularUiGrid']
        //},
        //{
        //    name: 'angularUiGridPtBr',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.language.pt-br.min',
        //    deps: ['angularUiGridCore'],
        //},
        //{
        //    name: 'angularUiGridTreeBase',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.tree-base.min',
        //    deps: ['angularUiGridCore']
        //},
        //{
        //    name: 'angularUiGridResizeColumns',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.resize-columns.min',
        //    deps: ['angularUiGridCore']
        //},
        //{
        //    name: 'angularUiGridMoveColumns',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.move-columns.min',
        //    deps: ['angularUiGridCore']
        //},
        //{
        //    name: 'angularUiGridGrouping',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.grouping.min',
        //    deps: ['angularUiGridTreeBase']
        //},
        //{
        //    name: 'angularUiGridExporter',
        //    path: 'assets/plugins/angular-ui-grid/ui-grid.exporter.min',
        //    deps: ['angularUiGridCore']
        //},
        //{
        //    name: 'download',
        //    path: 'assets/plugins/download-js/download'
        //}
    ];

    return plugins;
});