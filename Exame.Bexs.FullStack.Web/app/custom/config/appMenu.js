﻿define(function () {
    return [
        { icone: 'icon-doc', url: '#/', titulo: 'Home' },
        { icone: 'icon-magnifier', url: '#/grupos/lista', titulo: 'Grupos' },
        { icone: 'icon-grid', url: '#/painel/geral', titulo: 'Painel' },
        {
            icone: 'icon-book-open', titulo: 'Forms', menus: [
                {
                    icone: 'icon-book-open', url: '#/forms/lista', titulo: 'Lista'
                },
                {
                    icone: 'icon-book-open', url: '#/forms/detalhes', titulo: 'Detalhes'
                },
                { icone: 'icon-book-open', url: '#/forms/inclusao', titulo: 'Inclusão' },
                { icone: 'icon-book-open', url: '#/forms/edicao', titulo: 'Edição' },
                {
                    icone: 'icon-book-open', titulo: 'Plugins', menus: [
                        { icone: 'icon-book-open', url: '#/forms/mascaras', titulo: 'Máscaras' },
                        { icone: 'icon-book-open', url: '#/forms/filtros', titulo: 'Filtros' },
                        { icone: 'icon-book-open', url: '#/forms/datetimePickers', titulo: 'Datetime Pickers' },
                    ]
                }
            ]
        }
    ];
});