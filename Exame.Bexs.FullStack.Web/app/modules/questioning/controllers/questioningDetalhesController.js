define(function (require) {

    var ControllerBase = require('controllerBase');
    var controller = new ControllerBase('Questioning', 'Detalhes', 'id');

    angular
        .module(controller.moduleName)
        .registerController(controller.name, DetalhesController);

    // -------------------

    function DetalhesController($scope, $state, $stateParams, $injector, QuestioningService, ngNotify ) {

        $scope.maxCharacters = 2000;

        var getQuestioning = function () {
            $scope.answer = {};
            QuestioningService.getQuestioning($stateParams.id)
                .then(function (response) {
                    $scope.questioning = response.data.Questioning;
                    $scope.answers = response.data.Answers;
                })
        }
        getQuestioning();

        $scope.gravarResposta = function () {
            var model = {
                questioningId: $stateParams.id,
                user: $scope.answer.User,
                text: $scope.answer.Text
            };

            QuestioningService.postAnswer(model)
                .then(function (response) {
                    getQuestioning();
                    ngNotify.set('Respondido com sucesso!', 'success');
                })
        }

        $scope.answersLength = function (answersLength) {
            if (answersLength == 1) {
                return "resposta";
            }
            else {
                return "respostas"
            }
        }

        $scope.voltar = function () {
            $state.go(controller.moduleName + '.lista');
        }
    }
});

