define(function (require) {

    var ControllerBase = require('controllerBase');
    var controller = new ControllerBase('Questioning', 'Lista');

    angular
        .module(controller.moduleName)
        .registerController(controller.name, ListaController);

    // -------------------

    function ListaController($scope, $injector, $state, QuestioningService, ngNotify) {
        var modelService = $injector.get(controller.serviceName);

        $scope.maxCharacters = 2000;

        var getQuestionings = function () {
            $scope.item = {};
            QuestioningService.getQuestionings()
                .then(function (response) {
                    $scope.dates = response.data;
                })
        }
        getQuestionings();

        $scope.gravar = function () {
            QuestioningService.postQuestion($scope.item)
                .then(function (response) {
                    getQuestionings();
                    ngNotify.set('Pergunta inserida com sucesso!', 'success');
                })
        }

        $scope.details = function (id) {
            $state.go('questioning.detalhes', ({ id: id }));
        }

        $scope.selectedNoAnswers = function (model) {
            if ($scope.searchFilterNoAnswers) {
                if (model.Answers.length == 0) {
                    return model;
                }
                else {
                    return; 
                }
            }
            else {
                return model;
            }
        };

        $scope.answers = function (answersLength) {
            if (answersLength == 1) {
                return "resposta";
            }
            else {
                return "respostas"
            }
        }
    }
});
