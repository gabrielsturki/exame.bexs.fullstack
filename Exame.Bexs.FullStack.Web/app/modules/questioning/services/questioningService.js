﻿define(function () {
    var questioningService = function ($http) {
        // Url base universal
        var baseUrl = "Api/";

        this.getQuestionings = function () {
            return $http.get(baseUrl + "Questioning");
        }

        this.getSearch = function (list, search) {
            return $http.get(baseUrl + "Questioning?list="+ list + "&search=" + search);
        }

        this.getQuestioning = function (id) {
            return $http.get(baseUrl + "Questioning/Details?id=" + id);
        }

        this.postQuestion = function (model) {
            return $http.post(baseUrl + "Questioning/PostQuestion", model);
        }

        this.postAnswer = function (model) {
            return $http.post(baseUrl + "Answer/PostAnswer", model);
        }
    }
    return questioningService;
})