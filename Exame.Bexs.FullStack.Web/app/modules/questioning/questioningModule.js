﻿define(function (require) {
    var ComponentBase = require('componentBase');

    var component = new ComponentBase('Questioning');
    var app = angular.module(component.name, ['common']);

    app.config(function ($controllerProvider) {
        app.registerController = $controllerProvider.register;
    });

    require([
        'app/modules/questioning/services/questioningService.js',
    ], function (QuestioningService) {
        app.service('QuestioningService', QuestioningService);
    })

    app.config(function ($stateProvider) {
        component.states.forEach(function (item) {
            var state = component.configureState(item);
            $stateProvider.state(state);
        });
    });

    app.factory(component.serviceName, function ($resource) {
        return $resource('api/' + component.name + '/:id', null, {
            update: { method: 'PUT' }
        });
    });


});
