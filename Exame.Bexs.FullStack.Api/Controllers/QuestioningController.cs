﻿using Exame.Bexs.FullStack.Dal;
using Exame.Bexs.FullStack.Domain.DTO;
using Exame.Bexs.FullStack.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web.Http;

namespace Exame.Bexs.FullStack.Api.Controllers
{
    public class QuestioningController : ApiController
    {
        readonly ExameBexsFullStackContext context = ExameBexsFullStackContext.GetInstance();

        public IHttpActionResult Get()
        {
            //criação de duas listas para devolução de dados preenchidos para exibição na tela
            var dates = new List<DateQuestioningDto>();

            //pega todas as datas de todas as questoes e faz um distinct para nao repetir e existir um separador de datas
            var datesQuestionings = context.Questionings.Select(x => x.CreationDate).ToList().Distinct();

            //loop para controle e separaçao de questionamentos por data
            //para exibir na tela separado data por datas
            foreach (var date in datesQuestionings)
            {
                //consulta todos os questionamentos
                var questionings = context.Questionings.Where(x => x.CreationDate == date).ToList();

                var dateQuestioningDto = new DateQuestioningDto();
                dateQuestioningDto.CreationDate = date.ToShortDateString();

                //loop para alimentar lista de questionamentos 
                foreach (var questioning in questionings)
                {
                    var questioningDto = ControlQuestioning(questioning);
                    var answers = context.Answers.Where(x => x.QuestioningId == questioning.Id).ToList();

                    //loop para alimentar lista de respostas de acordo com a questão correspondente
                    foreach (var answer in answers)
                    {
                        var getListAnswers = questioningDto.Answers.Where(x => x.AnswerId == answer.Id && x.QuestioningId == questioning.Id).SingleOrDefault();
                        if (getListAnswers == null)
                        {
                            var answerDto = ControlAnswers(answer);
                            questioningDto.Answers.Add(answerDto);
                        }
                    }

                    //adiciona o objeto questionamento dentro da lista de questionamentos que pertence ao objeto de datas.
                    dateQuestioningDto.Questionings.Add(questioningDto);
                }

                //adiciona objeto com todos os dados na lista de datas e questionamentos
                dates.Add(dateQuestioningDto);
            }

            //retorna dados para tela
            return Ok(dates.OrderByDescending(x => x.CreationDate));
        }

        private QuestioningDto ControlQuestioning(Questioning questioning)
        {
            var questioningDto = new QuestioningDto();
            questioningDto.Id = questioning.Id;
            questioningDto.CreationDate = questioning.CreationDate;
            questioningDto.Text = questioning.Text;
            questioningDto.User = questioning.User;

            return questioningDto;
        }

        private AnswerDto ControlAnswers(Answer answer)
        {
            var answerDto = new AnswerDto();
            answerDto.AnswerId = answer.Id;
            answerDto.Text = answer.Text;
            answerDto.User = answer.User;
            answerDto.CreationDate = answer.CreationDate;
            answerDto.QuestioningId = answer.QuestioningId;

            return answerDto;
        }

        public IHttpActionResult GetDetails(int id)
        {
            var questioning = context.Questionings.Find(id);

            var answers = context.Answers.Where(x => x.QuestioningId == id).ToList();

            var result = new
            {
                Questioning = questioning,
                Answers = answers
            };

            return Ok(result);
        }

        public IHttpActionResult PostQuestion(QuestioningDto model)
        {
            var questioning = new Questioning();

            questioning.CreationDate = DateTime.Today;
            questioning.Text = model.Text;
            questioning.User = model.User;

            context.Questionings.Add(questioning);
            context.SaveChanges();

            return Ok(questioning);
        }
    }
}