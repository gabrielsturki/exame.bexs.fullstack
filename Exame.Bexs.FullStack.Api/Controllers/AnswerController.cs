﻿using Exame.Bexs.FullStack.Dal;
using Exame.Bexs.FullStack.Domain.DTO;
using Exame.Bexs.FullStack.Domain.Entities;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;

namespace Exame.Bexs.FullStack.Api.Controllers
{
    public class AnswerController : ApiController
    {
        private static ExameBexsFullStackContext context = ExameBexsFullStackContext.GetInstance();

        public IHttpActionResult GetAll(int questioningId)
        {
            var answers = context.Answers.Where(x => x.QuestioningId == questioningId).ToList();

            return Ok(answers);
        }

        [HttpPost]
        public IHttpActionResult PostAnswer(AnswerDto model)
        {
            var answer = new Answer();
            answer.CreationDate = DateTime.Today;
            answer.Text = model.Text;
            answer.User = model.User;
            answer.QuestioningId = model.QuestioningId;

            context.Answers.Add(answer);
            context.SaveChanges();

            return Ok(answer);
        }
    }
}
