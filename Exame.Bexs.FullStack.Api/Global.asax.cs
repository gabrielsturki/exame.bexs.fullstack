using Exame.Bexs.FullStack.Dal;
using System.Configuration;
using System.IO;
using System.Web.Http;

namespace Exame.Bexs.FullStack.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            DirectoryInfo d = new DirectoryInfo(@"C:/Temp");

            if (!d.Exists)
            {
                d.Create();
            }

            var connectionString = ConfigurationManager.ConnectionStrings["connection"].ToString();
            ExameBexsFullStackContext.SetConnectionString(connectionString);

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
