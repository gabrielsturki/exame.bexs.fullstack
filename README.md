## Rodar o projeto - necessário Visual Studio Express For Web ou Visual Studio Community

1. Faça o download do repositório
2. Extraia os arquivos em uma pasta de preferência
3. Execute o projeto clicando duas vezes no arquivo .sln
4. Assim que carregar, clique com o botão direito no projeto Web (Exame.Bexs.FullStack.Web) e defina-o como projeto de inicialização  
5. Clique com o botão direito na Solução e compile a aplicação.
6. Clique no botão Play na parte superior central do Visual Studio.

---

## Testes efetuados com o Postman

### GET:

http://localhost:52624/Api/Questioning
[
    {
        "CreationDate": "13/05/2020",
        "Questionings": [
            {
                "Id": 2,
                "User": "testeUsuario",
                "Text": "testeQuestao",
                "CreationDate": "2020-05-13T00:00:00-03:00",
                "Answers": [
                    {
                        "AnswerId": 2,
                        "User": "testeUsuarioResposta",
                        "Text": "testeResposta",
                        "CreationDate": "2020-05-13T00:00:00-03:00",
                        "QuestioningId": 2
                    }
                ]
            }
        ]
    }
]

http://localhost:52624/Api/Questioning/Details?id=2
{
    "Questioning": {
        "Id": 2,
        "Text": "testeQuestao",
        "User": "testeUsuario",
        "CreationDate": "2020-05-13T00:00:00-03:00"
    },
    "Answers": [
        {
            "Id": 2,
            "Text": "testeResposta",
            "User": "testeUsuarioResposta",
            "CreationDate": "2020-05-13T00:00:00-03:00",
            "QuestioningId": 2,
            "Questioning": {
                "Id": 2,
                "Text": "testeQuestao",
                "User": "testeUsuario",
                "CreationDate": "2020-05-13T00:00:00-03:00"
            }
        }
    ]
}

http://localhost:52624/Api/Answer?questioningId=2
[
    {
        "Id": 2,
        "Text": "testeResposta",
        "User": "testeUsuarioResposta",
        "CreationDate": "2020-05-13T00:00:00-03:00",
        "QuestioningId": 2,
        "Questioning": {
            "Id": 2,
            "Text": "testeQuestao",
            "User": "testeUsuario",
            "CreationDate": "2020-05-13T00:00:00-03:00"
        }
    }
]

### POST

http://localhost:52624/Api/Questioning/Question

{
    "Id": 4,
    "Text": "testeQuestaoPostman2",
    "User": "testeUsuarioPostman",
    "CreationDate": "2020-05-13T00:00:00-03:00"
}

http://localhost:52624/Api/Answer/Post
{
    "Id": 9,
    "Text": "testeQuestaoRespostaPostman",
    "User": "testeUsuarioRespostaPostman",
    "CreationDate": "2020-05-13T00:00:00-03:00",
    "QuestioningId": 3,
    "Questioning": {
        "Id": 3,
        "Text": "testeQuestaoPostman",
        "User": "testeUsuarioPostman",
        "CreationDate": "2020-05-13T00:00:00"
    }
}
